import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MaxTest {

    @Test
    public void maxTest(){
        int[] array = {1, 0, 4, 15, 2};
        int expected = 15;
        int actual = Main.max(array);
        assertEquals(expected, actual);
    }
}
